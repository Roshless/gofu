package api

import (
	"git.roshless.me/gofu/backend/config"
	"git.roshless.me/gofu/backend/middleware/jwt"
	"git.roshless.me/gofu/backend/model"
)

type Handlers struct {
	Users UserHandler
	Files FileHandler
}

func NewHandlers(cfg *config.Config, m *model.Models, jwto *jwt.JWTObject) Handlers {
	return Handlers{
		Users: UserHandler{
			registrationEnabled: cfg.RegistrationEnabled,
			model:               m.Users,
			j:                   *jwto,
		},

		Files: FileHandler{
			model: m.Files,
			j:     *jwto,
		},
	}
}
