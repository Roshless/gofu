package payloads

import "net/http"

type TokenResponse struct {
	Token string `json:"token"`
}

func NewTokenResponse(token string) *TokenResponse {
	return &TokenResponse{Token: token}
}

func (tr *TokenResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
