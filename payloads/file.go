package payloads

import (
	"errors"
	"net/http"

	"git.roshless.me/gofu/backend/model"
	"github.com/go-chi/render"
)

const (
	FileRequestFull = 0
)

type FileResponse struct {
	*model.File
}

func NewFileResponse(file model.File) *FileResponse {
	return &FileResponse{File: &file}
}

func NewFileListResponse(files []model.File) []render.Renderer {
	list := []render.Renderer{}
	for _, file := range files {
		list = append(list, NewFileResponse(file))
	}
	return list
}

func (f *FileResponse) Bind(r *http.Request) error {
	return nil
}

func (f *FileResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

type FileRequest struct {
	*model.File
	Type int
}

func (f *FileRequest) Bind(r *http.Request) error {
	if f.File == nil {
		return errors.New("missing file")
	}

	switch f.Type {
	case FileRequestFull:
		if err := f.IsCorrectlyInitialized(); err != nil {
			return err
		}
	}

	// Hard code in case of user trying to break things
	f.IsUploaded = false
	return nil
}
