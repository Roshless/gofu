package config

import (
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

var cfg *Config

type Config struct {
	FilesDirectory           string `yaml:"files_directory"`
	DatabaseConnectionString string `yaml:"database_connection_string"`
	Port                     string `yaml:"port"`
	MaxFileSizeBytes         int    `yaml:"max_file_size_bytes"`
	FrontendEnabled          bool   `yaml:"frontend_enabled"`
	RegistrationEnabled      bool   `yaml:"registration_enabled"`
	JWTSecretKey             string `yaml:"secret_key"`
	ApiUrl                   string `yaml:"api_url"`
}

func NewConfig(path string) *Config {
	config := &Config{}

	if path == "" {
		log.Fatalln("provide config path")
		return nil
	}

	file, err := os.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal(file, &config)
	if err != nil {
		log.Fatal(err)
	}

	cfg = config
	return cfg
}

func GetConfig() *Config {
	return cfg
}
