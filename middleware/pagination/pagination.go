package pagination

import (
	"context"
	"net/http"
	"strconv"

	"git.roshless.me/gofu/backend/payloads"
	"github.com/go-chi/render"
)

type (
	CustomKey string
)

const (
	PageIDKey    CustomKey = "page"
	PerPageIDKey CustomKey = "per_page"
)

func Paginate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		PageID := r.URL.Query().Get(string(PageIDKey))
		PerPage := r.URL.Query().Get(string(PerPageIDKey))
		var (
			err        error
			intPageID  = 0
			intPerPage = 30
		)
		if PageID != "" {
			// Only parsed to int to make sure this is a number
			intPageID, err = strconv.Atoi(PageID)
			if err != nil {
				render.Render(w, r, payloads.BadRequest(err))
				return
			}
		}
		if PerPage != "" {
			// Only parsed to int to make sure this is a number
			intPerPage, err = strconv.Atoi(PerPage)
			if err != nil {
				render.Render(w, r, payloads.BadRequest(err))
				return
			}
		}
		ctxTemp := context.WithValue(r.Context(), PageIDKey, intPageID)
		ctx := context.WithValue(ctxTemp, PerPageIDKey, intPerPage)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
