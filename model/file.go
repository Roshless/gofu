package model

import (
	"context"
	"errors"
	"net/netip"
	"regexp"
	"time"

	"github.com/google/uuid"
)

type File struct {
	ID          int        `json:"id"`
	Name        string     `json:"name"`
	SubmitterIP netip.Addr `json:"submitter_ip"`
	SubmitterID int        `json:"submitter_id"`
	IsUploaded  bool       `json:"is_uploaded"`
	ContentType string     `json:"content_type"`
	UploadDate  time.Time  `json:"upload_date"`
}

func (f *File) IsCorrectlyInitialized() error {
	if f.Name == "" {
		f.Name = uuid.NewString()
	} else {
		if len(f.Name) < 5 {
			return errors.New("name too short")
		}

		if len(f.Name) > 64 {
			return errors.New("name too long")
		}

		if !regexp.MustCompile("^[A-Za-z0-9-.]+$").MatchString(f.Name) {
			return errors.New("regexp on name failed")
		}
	}

	return nil
}

type FileModel struct {
	DB PgxIface
	FileModelInterface
}

func (p FileModel) Add(ctx context.Context, item *File) error {
	sql := `
	INSERT INTO files(
		name, submitter_ip, submitter_id, is_uploaded, upload_date
	) values($1, $2, $3, $4, $5)
	`

	_, err := p.DB.Exec(ctx, sql, item.Name, item.SubmitterIP, item.SubmitterID, item.IsUploaded, item.UploadDate)
	return errorConvert(err)
}

func (p FileModel) PostUploadFileUpdate(ctx context.Context, userID int, name, contentType string) error {
	sql := `
	UPDATE files 
	SET is_uploaded = true, content_type = $1
	WHERE submitter_id = $2 AND name = $3
	`

	_, err := p.DB.Exec(ctx, sql, contentType, userID, name)
	return errorConvert(err)
}

func (p FileModel) Get(ctx context.Context, name string) (File, error) {
	sql := `
	SELECT id, name, submitter_ip, submitter_id, is_uploaded, content_type, upload_date
	FROM files
	WHERE name = $1
	`

	row := p.DB.QueryRow(ctx, sql, name)

	var item File
	err := row.Scan(&item.ID, &item.Name, &item.SubmitterIP, &item.SubmitterID,
		&item.IsUploaded, &item.ContentType, &item.UploadDate)

	// Return SQL error
	return item, errorConvert(err)
}

func (p FileModel) GetWithAuth(ctx context.Context, name, userID string) (File, error) {
	sql := `
	SELECT id, name, submitter_ip, submitter_id, is_uploaded, content_type, upload_date
	FROM files
	WHERE name = $1 AND submitter_id = $2
	`

	row := p.DB.QueryRow(ctx, sql, name, userID)

	var item File
	err := row.Scan(&item.ID, &item.Name, &item.SubmitterIP, &item.SubmitterID,
		&item.IsUploaded, &item.ContentType, &item.UploadDate)

	// Return SQL error
	return item, errorConvert(err)
}

func (p FileModel) CheckIfExists(ctx context.Context, name string) (bool, error) {
	sql := `
	SELECT EXISTS(SELECT 1 FROM files WHERE name = $1)
	`

	row := p.DB.QueryRow(ctx, sql, name)

	var value bool
	err := row.Scan(&value)

	// Wrapper not needed- only returns bool on successful execution
	return value, err
}

func (p FileModel) Delete(ctx context.Context, name, userID string) (int, error) {
	sql := `
	DELETE FROM files
	WHERE name = $1 AND submitter_id = $2
	`

	tag, err := p.DB.Exec(ctx, sql, name, userID)
	return int(tag.RowsAffected()), errorConvert(err)
}

// GetAll returns all files for userID but without content.
// Now with pagination!
func (p FileModel) GetAll(ctx context.Context, userID, pageID, limit int) ([]File, error) {
	sql := `
	SELECT id, name, submitter_ip, submitter_id, is_uploaded, content_type, upload_date
	FROM files
	WHERE submitter_id = $1
	ORDER BY upload_date
	OFFSET $2
	LIMIT $3
	`

	if pageID < 0 {
		pageID = 0
	}

	rows, err := p.DB.Query(ctx, sql, userID, pageID, limit)
	if err != nil {
		return nil, errorConvert(err)
	}

	var items []File
	for rows.Next() {
		var f File
		err = rows.Scan(&f.ID, &f.Name, &f.SubmitterIP, &f.SubmitterID,
			&f.IsUploaded, &f.ContentType, &f.UploadDate)
		if err != nil {
			return nil, errorConvert(err)
		}
		items = append(items, f)
	}
	if err = rows.Err(); err != nil {
		return nil, errorConvert(err)
	}
	return items, errorConvert(err)
}

func (p FileModel) GetEveryFile(ctx context.Context, pageID, limit int) ([]File, error) {
	sql := `
	SELECT id, name, submitter_ip, submitter_id, is_uploaded, content_type, upload_date
	FROM files
	ORDER BY upload_date
	OFFSET $1
	LIMIT $2
	`

	if pageID < 0 {
		pageID = 0
	}

	rows, err := p.DB.Query(ctx, sql, pageID, limit)
	if err != nil {
		return nil, errorConvert(err)
	}

	var items []File
	for rows.Next() {
		var f File
		err = rows.Scan(&f.ID, &f.Name, &f.SubmitterIP, &f.SubmitterID,
			&f.IsUploaded, &f.ContentType, &f.UploadDate)
		if err != nil {
			return nil, errorConvert(err)
		}
		items = append(items, f)
	}
	if err = rows.Err(); err != nil {
		return nil, errorConvert(err)
	}
	return items, errorConvert(err)
}

func (p FileModel) UpdateContentType(ctx context.Context, name, contentType string) error {
	sql := `
	UPDATE files
	SET content_type = $1
	WHERE name = $2
	`

	_, err := p.DB.Exec(ctx, sql, contentType, name)
	return errorConvert(err)
}
