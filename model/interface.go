package model

import (
	"context"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
)

type PgxIface interface {
	Begin(context.Context) (pgx.Tx, error)
	Exec(context.Context, string, ...interface{}) (pgconn.CommandTag, error)
	QueryRow(context.Context, string, ...interface{}) pgx.Row
	Query(context.Context, string, ...interface{}) (pgx.Rows, error)
	Ping(context.Context) error
	Close()
}

type UserModelInterface interface {
	Add(ctx context.Context, u User) error
	Get(ctx context.Context, name string) (User, error)
	GetAll(ctx context.Context) ([]User, error)
	Delete(ctx context.Context, name string) error
	Update(ctx context.Context, name string, u User) error
	Authenticate(ctx context.Context, u User) (User, error)
}

type FileModelInterface interface {
	Add(ctx context.Context, p File) error
	SetContentFlag(ctx context.Context, name string) error
	Get(ctx context.Context, name string) (File, error)
	GetAll(ctx context.Context) ([]File, error)
	Delete(ctx context.Context, name string) error
}
