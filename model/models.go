package model

type Models struct {
	Users       UserModel
	Files       FileModel
}

func NewModels(db PgxIface) Models {
	return Models{
		Users:       UserModel{DB: db},
		Files:       FileModel{DB: db},
	}
}
