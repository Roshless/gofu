package model

import (
	"errors"

	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
)

var (
	ErrNotFound      = errors.New("element not found in database")
	ErrConflict      = errors.New("element conflict in database")
	ErrMissingFields = errors.New("missing json fields")
	ErrNoNewEntries  = errors.New("no new entries")
)

func errorConvert(err error) error {
	if err != nil {
		var pgErr *pgconn.PgError
		if errors.As(err, &pgErr) {
			if pgErr.Code == pgerrcode.UniqueViolation {
				err = ErrConflict
			}
		} else if err == pgx.ErrNoRows {
			err = ErrNotFound
		}
	}
	return err
}
