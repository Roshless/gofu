package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"time"

	"git.roshless.me/gofu/backend/config"
	"git.roshless.me/gofu/backend/controller"
	"github.com/jackc/pgx/v5/pgxpool"
)

func connectDatabase(cfg *config.Config) (*pgxpool.Pool, error) {
	var pool *pgxpool.Pool
	poolConfig, err := pgxpool.ParseConfig(cfg.DatabaseConnectionString)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	pool, err = pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil {
		return nil, err
	}

	return pool, nil
}

func main() {
	defaultConfigPath := "/config.yml"

	configPath := flag.String("config", defaultConfigPath, "config path")
	flag.Parse()
	if *configPath == defaultConfigPath {
		envValue := os.Getenv("CONFIG_PATH")
		if envValue != "" {
			*configPath = envValue
		}
	}

	cfg := config.NewConfig(*configPath)

	db, err := connectDatabase(cfg)
	if err != nil {
		panic(err)
	}

	c := controller.NewController(cfg, db)

	c.MountHandlers()
	http.ListenAndServe(":"+c.Config.Port, c.Router)

}
