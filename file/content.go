package file

import (
	"os"
	"path"

	"git.roshless.me/gofu/backend/config"
	"git.roshless.me/gofu/backend/model"
	"github.com/gabriel-vasile/mimetype"
)

func WriteContent(item *model.File, content *[]byte) error {
	folder := config.GetConfig().FilesDirectory

	err := os.MkdirAll(folder, os.ModePerm)
	if err != nil {
		return err
	}

	err = os.WriteFile(path.Join(folder, item.Name), *content, 0644)
	if err != nil {
		return err
	}
	return nil
}

func ReadContent(item *model.File) ([]byte, error) {
	folder := config.GetConfig().FilesDirectory

	bytes, err := os.ReadFile(path.Join(folder, item.Name))
	if err != nil {
		return nil, err
	}
	return bytes, nil
}

func DeleteContent(item *model.File) error {
	folder := config.GetConfig().FilesDirectory

	err := os.Remove(path.Join(folder, item.Name))
	if err != nil {
		return err
	}
	return nil
}

func DetectContentType(item *model.File) (string, error) {
	folder := config.GetConfig().FilesDirectory

	mtype, err := mimetype.DetectFile(path.Join(folder, item.Name))
	return mtype.String(), err
}
